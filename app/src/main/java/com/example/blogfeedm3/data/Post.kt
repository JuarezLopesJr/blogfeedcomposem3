package com.example.blogfeedm3.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Post(
    val id: String,
    val title: String,
    val author: String,
    val image: String,
    val excerpt: String,
    val publishDate: String,
) : Parcelable