package com.example.blogfeedm3.utils

import com.example.blogfeedm3.data.Post

object PostFactory {
    fun makePosts(): List<Post> {
        return listOf(
            Post(
                "1",
                "Beyond Android",
                "Jaiminho",
                "https://picsum.photos/800/400",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            ),
            Post(
                "2",
                "The Constraint Layout",
                "Jaiminho",
                "https://picsum.photos/800/600",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            ),
            Post(
                "3",
                "The end of XML",
                "Jaiminho",
                "https://picsum.photos/900/400",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            ),
            Post(
                "4",
                "Diving into Jetpack Compose",
                "Jaiminho",
                "https://picsum.photos/600/400",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            ),
            Post(
                "5",
                "Compose + KMP",
                "Jaiminho",
                "https://picsum.photos/500/500",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            ),
            Post(
                "6",
                "Tackling Android Performance",
                "Jaiminho",
                "https://picsum.photos/700/600",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            ),
            Post(
                "7",
                "Accessibility Patterns",
                "Jaiminho",
                "https://picsum.photos/800/700",
                "Mussum Ipsum, cacilds vidis litro abertis. Detraxit consequat et quo num tendi nada.Suco de cevadiss deixa as pessoas mais interessantis.Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mim que vai caçá sua turmis!\n" +
                        "\n" +
                        "Delegadis gente finis, bibendum egestas augue arcu ut est.Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Si num tem leite então bota uma pinga aí cumpadi!Manduma pindureta quium dia nois paga.\n" +
                        "\n" +
                        "Atirei o pau no gatis, per gatis num morreus.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis.Mé faiz elementum girarzis, nisi eros vermeio.\n" +
                        "\n" +
                        "Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.Leite de capivaris, leite de mula manquis sem cabeça.Quem num gosta di mé, boa gentis num é.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.\n" +
                        "\n" +
                        "Admodum accumsan disputationi eu sit. Vide electram sadipscing et per.Não sou faixa preta cumpadi, sou preto inteiris, inteiris.Detraxit consequat et quo num tendi nada.Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.",
                "17/06/2023",
            )
        )
    }
}