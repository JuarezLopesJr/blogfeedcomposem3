package com.example.blogfeedm3.ui.screens.post

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.AsyncImage
import com.example.blogfeedm3.data.Post

@Composable
fun PostItem(
    modifier: Modifier = Modifier,
    post: Post
) {
    /* heightIn gives room to the Card expand in the vertical if necessary */
    Card(modifier = modifier.heightIn(min = 265.dp)) {
        ConstraintLayout(modifier = Modifier.fillMaxWidth()) {
            val (
                header,
                excerpt,
                author,
                title,
                date,
            ) = createRefs()

            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(140.dp)
                    .constrainAs(header) {
                        start.linkTo(parent.start)
                        top.linkTo(parent.top)
                    },
                model = post.image,
                contentScale = ContentScale.Crop,
                contentDescription = null,
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.Black.copy(alpha = 0.4f))
                    .padding(horizontal = 12.dp, vertical = 6.dp)
                    .constrainAs(title) {
                        bottom.linkTo(header.bottom)
                    },
                text = post.title,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Start,
                color = Color.White,
            )

            Text(
                modifier = Modifier
                    .constrainAs(excerpt) {
                        top.linkTo(header.bottom, margin = 12.dp)
                        start.linkTo(title.start)
                    }
                    .padding(horizontal = 12.dp),
                text = post.excerpt,
                maxLines = 4,
                overflow = TextOverflow.Ellipsis
            )

            Text(
                modifier = Modifier
                    .padding(all = 12.dp)
                    .constrainAs(date) {
                        top.linkTo(excerpt.bottom)
                        end.linkTo(parent.end)
                        bottom.linkTo(parent.bottom)
                    },
                text = post.publishDate,
                fontSize = 14.sp
            )

            Text(
                modifier = Modifier
                    .padding(all = 12.dp)
                    .constrainAs(author) {
                        bottom.linkTo(parent.bottom)
                        end.linkTo(date.start)
                    },
                text = post.author,
                fontSize = 14.sp
            )
            /* distributes the card width evenly between the defined composables */
            createHorizontalChain(author, date, chainStyle = ChainStyle.SpreadInside)
        }
    }
}