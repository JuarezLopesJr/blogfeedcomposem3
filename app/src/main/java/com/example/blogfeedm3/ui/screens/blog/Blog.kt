package com.example.blogfeedm3.ui.screens.blog

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.blogfeedm3.R
import com.example.blogfeedm3.data.Post
import com.example.blogfeedm3.ui.screens.destinations.PostDetailDestination
import com.example.blogfeedm3.ui.screens.post.PostItem
import com.example.blogfeedm3.utils.PostFactory
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@RootNavGraph(start = true)
@Destination
@Composable
fun Blog(
    modifier: Modifier = Modifier,
    posts: List<Post> = PostFactory.makePosts(),
    navigator: DestinationsNavigator,
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(all = 16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(items = posts, key = { it.id }) { post ->
            PostItem(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClickLabel = stringResource(R.string.cd_read_post)) {
                        navigator.navigate(PostDetailDestination(post))
                    },
                post = post
            )
        }
    }
}